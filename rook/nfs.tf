resource "kubectl_manifest" "cephnfs-pool" {
  depends_on = [kubectl_manifest.ceph_cluster]
  yaml_body = yamlencode({
    apiVersion = "ceph.rook.io/v1"
    kind       = "CephBlockPool"
    metadata = {
      name      = "ceph-nfs"
      namespace = one(kubernetes_namespace.rook.metadata).name
    }
    spec = {
      name          = ".nfs" # hard-coded
      failureDomain = "osd"
      replicated = {
        size = 2
      }
    }
  })
}

# Use kubectl wait directly to wait for the bucket to be "Bound" before moving
# on.
resource "null_resource" "wait_for_nfs_pool_ready" {
  depends_on = [kubectl_manifest.cephnfs-pool]
  triggers = {
    pool_uid = kubectl_manifest.cephnfs-pool.uid
  }

  provisioner "local-exec" {
    command = join(" ", ["sleep", "30"])
    #command = join(" ", [
    #  "kubectl wait",
    #  "--for=jsonpath=.status.phase=Ready",
    #  "--namespace=${one(kubernetes_namespace.rook.metadata).name}",
    #  "${kubectl_manifest.cephnfs-pool.kind}/${kubectl_manifest.cephnfs-pool.name}"
    #])
  }
}

resource "kubectl_manifest" "cephnfs" {
  depends_on = [
    kubectl_manifest.cephfs,
    null_resource.wait_for_nfs_pool_ready,
  ]
  yaml_body = yamlencode({
    apiVersion = "ceph.rook.io/v1"
    kind       = "CephNFS"
    metadata = {
      name      = "cephnfs"
      namespace = one(kubernetes_namespace.rook.metadata).name
    }
    spec = {
      server = {
        active = 1

        #resources = {
        #  limits = {
        #    cpu    = "1"
        #    memory = "2Gi"
        #  }
        #  requests = {
        #    cpu    = "0"
        #    memory = "0Gi"
        #  }
        #}

        logLevel = "NIV_INFO"
      }
    }
  })

  wait = true

  #lifecycle {
  #  prevent_destroy = true
  #}
}

resource "kubernetes_service" "cephnfs-lb" {
  depends_on = [kubectl_manifest.cephnfs]
  metadata {
    namespace   = one(kubernetes_namespace.rook.metadata).name
    name        = "rook-ceph-nfs-lb"
    annotations = var.loadbalancer_annotations
  }
  spec {
    type = "LoadBalancer"
    selector = {
      app      = "rook-ceph-nfs"
      ceph_nfs = kubectl_manifest.cephnfs.name
      instance = "a"
    }
    port {
      name = "nfs"
      port = 2049
    }
  }
}
