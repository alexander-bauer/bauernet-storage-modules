variable "cert_annotations" {
  type = map(string)
}

variable "rgw_fqdn" {
  type = string
}

variable "debug_operator" {
  description = "Set the DEBUG log level for the rook-ceph operator"
  type        = bool
  default     = false
}

variable "loadbalancer_annotations" {
  description = "LoadBalancer service annotations, such as for MetalLB"
  type        = map(string)
  default     = {}
}
