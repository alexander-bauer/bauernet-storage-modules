resource "kubectl_manifest" "cephfs" {
  depends_on = [kubectl_manifest.ceph_cluster]
  yaml_body = yamlencode({
    apiVersion = "ceph.rook.io/v1"
    kind       = "CephFilesystem"
    metadata = {
      name      = "cephfs"
      namespace = one(kubernetes_namespace.rook.metadata).name
    }
    spec = {
      metadataPool = {
        failureDomain = "osd"
        replicated    = { size = 2 }
      }
      dataPools = [
        { name = "main", failureDomain = "osd", replicated = { size = 2 } }
      ]
      preserveFilesystemOnDelete = true
      metadataServer = {
        activeCount   = 1
        activeStandby = true
      }
    }
  })

  wait = true

  lifecycle {
    prevent_destroy = true
  }
}

resource "kubernetes_storage_class" "cephfs" {
  metadata {
    name = "cephfs"
  }
  storage_provisioner = "${one(kubernetes_namespace.rook.metadata).name}.cephfs.csi.ceph.com"
  parameters = {
    # clusterID is the namespace where the rook ceph cluster is running
    clusterID = one(kubernetes_namespace.rook.metadata).name

    fsName = yamldecode(kubectl_manifest.cephfs.yaml_body).metadata.name
    pool = join("-", [
      yamldecode(kubectl_manifest.cephfs.yaml_body).metadata.name,
      one(yamldecode(kubectl_manifest.cephfs.yaml_body).spec.dataPools).name
    ])

    # The secrets contain Ceph admin credentials.
    "csi.storage.k8s.io/provisioner-secret-name"            = "rook-csi-cephfs-provisioner"
    "csi.storage.k8s.io/provisioner-secret-namespace"       = one(kubernetes_namespace.rook.metadata).name
    "csi.storage.k8s.io/controller-expand-secret-name"      = "rook-csi-cephfs-provisioner"
    "csi.storage.k8s.io/controller-expand-secret-namespace" = one(kubernetes_namespace.rook.metadata).name
    "csi.storage.k8s.io/node-stage-secret-name"             = "rook-csi-cephfs-node"
    "csi.storage.k8s.io/node-stage-secret-namespace"        = one(kubernetes_namespace.rook.metadata).name
  }

  lifecycle {
    prevent_destroy = true
  }
}


resource "kubectl_manifest" "cephfs_volumesnapshotclass" {
  depends_on = [kubernetes_storage_class.cephfs]
  yaml_body = yamlencode({
    apiVersion = "snapshot.storage.k8s.io/v1"
    kind       = "VolumeSnapshotClass"
    metadata = {
      name = "csi-cephfsplugin-snapclass"
      annotations = {
        "snapshot.storage.kubernetes.io/is-default-class" = "true"
      }
      labels = {
        "velero.io/csi-volumesnapshot-class" = "true"
      }
    }
    driver = "rook-ceph.cephfs.csi.ceph.com" # driver:namespace:operator
    parameters = {
      # Specify a string that identifies your cluster. Ceph CSI supports any
      # unique string. When Ceph CSI is deployed by Rook use the Rook namespace,
      # for example "rook-ceph".
      clusterID                                         = one(kubernetes_namespace.rook.metadata).name # namespace:cluster
      "csi.storage.k8s.io/snapshotter-secret-name"      = "rook-csi-cephfs-provisioner"
      "csi.storage.k8s.io/snapshotter-secret-namespace" = one(kubernetes_namespace.rook.metadata).name
    }
    deletionPolicy = "Delete"
  })

  lifecycle {
    prevent_destroy = true
  }
}
