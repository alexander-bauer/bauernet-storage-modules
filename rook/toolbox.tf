data "http" "toolbox_manifest" {
  url = "https://raw.githubusercontent.com/rook/rook/release-1.11/deploy/examples/toolbox.yaml"
}

resource "kubectl_manifest" "ceph_toolbox" {
  yaml_body = data.http.toolbox_manifest.response_body

  lifecycle {
    precondition {
      condition     = data.http.toolbox_manifest.status_code == 200
      error_message = "Bad status code"
    }
  }
}
