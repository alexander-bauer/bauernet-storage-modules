output "default_storage_class" {
  value = one(kubernetes_storage_class.ceph_block.metadata).name
}

output "ceph" {
  value = {
    block  = one(kubernetes_storage_class.ceph_block.metadata).name
    fs     = one(kubernetes_storage_class.cephfs.metadata).name
    bucket = one(kubernetes_storage_class.ceph_bucket.metadata).name
  }
}

output "snapshots" {
  value = {
    block = {
      name   = nonsensitive(yamldecode(kubectl_manifest.ceph_block_volumesnapshotclass.yaml_body).metadata.name)
      driver = nonsensitive(yamldecode(kubectl_manifest.ceph_block_volumesnapshotclass.yaml_body).driver)
    }
    fs = {
      name   = nonsensitive(yamldecode(kubectl_manifest.cephfs_volumesnapshotclass.yaml_body).metadata.name)
      driver = nonsensitive(yamldecode(kubectl_manifest.cephfs_volumesnapshotclass.yaml_body).driver)
    }
  }
}

output "metrics" {
  value = {
    service_monitors = [
      {
        metadata = {
          name      = "rook-mgr"
          namespace = one(kubernetes_namespace.rook.metadata).name
        }
        spec = {
          selector = {
            matchLabels = {
              app = "csi-metrics"
            }
          }
          endpoints = [{
            port = "csi-http-metrics"
          }]
          targetLabels = [
            "rook-ceph",
            "csi",
          ]
        }
      },
      {
        metadata = {
          name      = "csi"
          namespace = one(kubernetes_namespace.rook.metadata).name
        }
        spec = {
          selector = {
            matchLabels = {
              app = "csi-metrics"
            }
          }
          endpoints = [{
            port = "csi-http-metrics"
          }]
          targetLabels = [
            "rook-ceph",
            "csi",
          ]
        }
      },
    ]
  }
}

output "namespace" {
  value = one(kubernetes_namespace.rook.metadata).name
}
output "config_secret" {
  value = "rook-ceph-config"
}
output "admin_keyring_secret" {
  value = "rook-ceph-admin-keyring"
}
