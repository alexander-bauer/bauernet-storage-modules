resource "kubectl_manifest" "ceph_block" {
  depends_on = [kubectl_manifest.ceph_cluster]
  yaml_body = yamlencode({
    apiVersion = "ceph.rook.io/v1"
    kind       = "CephBlockPool"
    metadata = {
      name      = "ceph-block"
      namespace = one(kubernetes_namespace.rook.metadata).name
    }
    spec = {
      failureDomain = "osd"
      replicated    = { size = 2 }
      deviceClass   = "hdd"
      parameters = {   # direct pg parameters
        min_size = "1" # minimum number of replicas for IO
      }
    }
  })

  wait = true

  lifecycle {
    prevent_destroy = true
  }
}

resource "kubernetes_storage_class" "ceph_block" {
  metadata {
    name = "ceph-block"
    annotations = {
      "storageclass.kubernetes.io/is-default-class" = true
    }
  }
  storage_provisioner = "${one(kubernetes_namespace.rook.metadata).name}.rbd.csi.ceph.com"
  parameters = {
    # clusterID is the namespace where the rook ceph cluster is running
    clusterID = one(kubernetes_namespace.rook.metadata).name
    pool      = yamldecode(kubectl_manifest.ceph_block.yaml_body).metadata.name

    # RBD image features. Available for imageFormat: "2". CSI RBD currently supports only `layering` feature.
    imageFeatures = "layering"

    # The secrets contain Ceph admin credentials.
    "csi.storage.k8s.io/provisioner-secret-name"            = "rook-csi-rbd-provisioner"
    "csi.storage.k8s.io/provisioner-secret-namespace"       = one(kubernetes_namespace.rook.metadata).name
    "csi.storage.k8s.io/controller-expand-secret-name"      = "rook-csi-rbd-provisioner"
    "csi.storage.k8s.io/controller-expand-secret-namespace" = one(kubernetes_namespace.rook.metadata).name
    "csi.storage.k8s.io/node-stage-secret-name"             = "rook-csi-rbd-node"
    "csi.storage.k8s.io/node-stage-secret-namespace"        = one(kubernetes_namespace.rook.metadata).name

    # Specify the filesystem type of the volume. If not specified, csi-provisioner
    # will set default as `ext4`. Note that `xfs` is not recommended due to potential deadlock
    # in hyperconverged settings where the volume is mounted on the same node as the osds.
    "csi.storage.k8s.io/fstype" : "ext4"
  }
  allow_volume_expansion = true

  lifecycle {
    prevent_destroy = true
  }
}

resource "kubectl_manifest" "ceph_block_volumesnapshotclass" {
  depends_on = [kubernetes_storage_class.ceph_block]
  yaml_body = yamlencode({
    apiVersion = "snapshot.storage.k8s.io/v1"
    kind       = "VolumeSnapshotClass"
    metadata = {
      name = "csi-rbdplugin-snapclass"
      annotations = {
        "snapshot.storage.kubernetes.io/is-default-class" = "true"
      }
      labels = {
        "velero.io/csi-volumesnapshot-class" = "true"
      }
    }
    driver = "rook-ceph.rbd.csi.ceph.com" # driver:namespace:operator
    parameters = {
      # Specify a string that identifies your cluster. Ceph CSI supports any
      # unique string. When Ceph CSI is deployed by Rook use the Rook namespace,
      # for example "rook-ceph".
      clusterID                                         = one(kubernetes_namespace.rook.metadata).name # namespace:cluster
      "csi.storage.k8s.io/snapshotter-secret-name"      = "rook-csi-rbd-provisioner"
      "csi.storage.k8s.io/snapshotter-secret-namespace" = one(kubernetes_namespace.rook.metadata).name
    }
    deletionPolicy = "Delete"
  })

  lifecycle {
    prevent_destroy = true
  }
}
