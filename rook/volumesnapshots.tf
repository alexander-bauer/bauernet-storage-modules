locals {
  url_base = "https://github.com/kubernetes-csi/external-snapshotter/raw/master"
  crd_files = [
    "client/config/crd/snapshot.storage.k8s.io_volumesnapshots.yaml",
    "client/config/crd/snapshot.storage.k8s.io_volumesnapshotcontents.yaml",
    "client/config/crd/snapshot.storage.k8s.io_volumesnapshotclasses.yaml",
  ]
  snapshot_controller_files = [
    "deploy/kubernetes/snapshot-controller/rbac-snapshot-controller.yaml",
    "deploy/kubernetes/snapshot-controller/setup-snapshot-controller.yaml",
  ]
}

data "http" "crds" {
  for_each = toset(local.crd_files)
  url      = "${local.url_base}/${each.value}"
  request_headers = {
    Accept = "text/plain"
  }
}

data "http" "snapshot_controller" {
  for_each = toset(local.snapshot_controller_files)
  url      = "${local.url_base}/${each.value}"
  request_headers = {
    Accept = "text/plain"
  }
}

data "kubectl_file_documents" "crds" {
  for_each = data.http.crds
  content  = each.value.response_body
}

data "kubectl_file_documents" "snapshot_controller" {
  for_each = data.http.snapshot_controller
  content  = each.value.response_body
}

resource "kubectl_manifest" "crds" {
  for_each      = merge([for obj in data.kubectl_file_documents.crds : obj.manifests]...)
  yaml_body     = each.value
  ignore_fields = ["status"]
}

resource "kubectl_manifest" "snapshot_controller" {
  for_each      = merge([for obj in data.kubectl_file_documents.snapshot_controller : obj.manifests]...)
  yaml_body     = each.value
  ignore_fields = ["status"]
}
