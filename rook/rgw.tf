resource "kubectl_manifest" "rgw" {
  depends_on = [kubectl_manifest.ceph_cluster]
  yaml_body = yamlencode({
    apiVersion = "ceph.rook.io/v1"
    kind       = "CephObjectStore"
    metadata = {
      name      = "rgw"
      namespace = one(kubernetes_namespace.rook.metadata).name
    }
    spec = {
      metadataPool = {
        failureDomain = "osd"
        replicated    = { size = 2 }
      }
      dataPool = {
        failureDomain = "osd"
        replicated    = { size = 2 }
      }
      gateway = {
        sslCertificateRef = null
        port              = 80
        instances         = 1
      }
      preservePoolsOnDelete = true
      healthCheck = {
        bucket = {
          disabled = false
          interval = "60s"
        }
      }
    }
  })

  wait = true

  lifecycle {
    prevent_destroy = true
  }
}

resource "kubernetes_storage_class" "ceph_bucket" {
  metadata {
    name = "ceph-bucket"
  }
  storage_provisioner = "${one(kubernetes_namespace.rook.metadata).name}.ceph.rook.io/bucket"
  parameters = {
    objectStoreName      = yamldecode(kubectl_manifest.rgw.yaml_body).metadata.name
    objectStoreNamespace = one(kubernetes_namespace.rook.metadata).name
  }
  reclaim_policy = "Delete"

  lifecycle {
    prevent_destroy = true
  }
}

data "kubernetes_service" "rgw" {
  metadata {
    namespace = one(kubernetes_namespace.rook.metadata).name
    name      = "rook-ceph-rgw-rgw"
  }
}

resource "kubernetes_ingress_v1" "rgw" {
  metadata {
    namespace   = one(kubernetes_namespace.rook.metadata).name
    name        = "ceph-rgw"
    annotations = var.cert_annotations
  }
  spec {
    rule {
      host = var.rgw_fqdn
      http {
        path {
          path = "/"
          backend {
            service {
              name = one(data.kubernetes_service.rgw.metadata).name
              port {
                number = one(one(data.kubernetes_service.rgw.spec).port).port
              }
            }
          }
        }
      }
    }
    tls {
      secret_name = "rgw-tls"
      hosts       = [var.rgw_fqdn]
    }
  }
}

locals {
  object_storage_service      = one(data.kubernetes_service.rgw.metadata).name
  object_storage_service_host = "${one(data.kubernetes_service.rgw.metadata).name}.${one(kubernetes_namespace.rook.metadata).name}.svc"
  object_storage = {
    storage_class = one(kubernetes_storage_class.ceph_bucket.metadata).name
    service       = local.object_storage_service
    internal_host = local.object_storage_service_host
    internal_url  = "http://${local.object_storage_service_host}"
    external_url  = "https://${var.rgw_fqdn}"
  }
}

output "object" {
  description = "Details for RGW object storage"
  value       = local.object_storage
}

#resource "kubectl_manifest" "object_admin" {
#  force_new = true # capabilities cannot be changed
#  yaml_body = yamlencode({
#    apiVersion = "ceph.rook.io/v1"
#    kind       = "CephObjectStoreUser"
#    metadata = {
#      namespace = one(kubernetes_namespace.rook.metadata).name
#      name      = "admin"
#    }
#    spec = {
#      store       = yamldecode(kubectl_manifest.rgw.yaml_body).metadata.name
#      displayName = "admin"
#      capabilities = {
#        user     = "*"
#        bucket   = "*"
#        metadata = "*"
#        usage    = "*"
#        zone     = "*"
#      }
#    }
#  })
#}
#
## Use kubectl wait directly to wait for the user to be ready.
#resource "null_resource" "wait_for_object_admin" {
#  depends_on = [kubectl_manifest.object_admin]
#  triggers = {
#    user_uid = kubectl_manifest.object_admin.uid
#  }
#
#  provisioner "local-exec" {
#    command = join(" ", [
#      "kubectl wait",
#      "--for=jsonpath=.status.phase=Ready",
#      "--namespace=${one(kubernetes_namespace.rook.metadata).name}",
#      "${kubectl_manifest.object_admin.kind}/${kubectl_manifest.object_admin.name}"
#    ])
#  }
#}
#
#data "kubernetes_secret" "object_admin" {
#  depends_on = [null_resource.wait_for_object_admin]
#  metadata {
#    namespace = one(kubernetes_namespace.rook.metadata).name
#    name      = "rook-ceph-object-user-rgw-admin"
#  }
#}
#
#
#output "object_admin" {
#  description = "Credentials to an admin user for object storage"
#  sensitive   = true
#  value = {
#    endpoint   = "https://${var.rgw_fqdn}"
#    access_key = data.kubernetes_secret.object_admin.data.AccessKey
#    secret_key = data.kubernetes_secret.object_admin.data.SecretKey
#  }
#}
#
#output "object_admin_minio" {
#  description = "Admin "
#  sensitive   = true
#  value = {
#    url       = "https://${var.rgw_fqdn}"
#    accessKey = data.kubernetes_secret.object_admin.data.AccessKey
#    secretKey = data.kubernetes_secret.object_admin.data.SecretKey
#    api       = "s3v4"
#    path      = "auto"
#  }
#}
