resource "kubectl_manifest" "mgr_pool" {
  depends_on = [kubectl_manifest.ceph_cluster]
  yaml_body = yamlencode({
    apiVersion = "ceph.rook.io/v1"
    kind       = "CephBlockPool"
    metadata = {
      # If the built-in Ceph pool used by the Ceph mgr needs to be configured with alternate
      # settings, create this pool with any of the pool properties. Create this pool immediately
      # with the cluster CR, or else some properties may not be applied when Ceph creates the
      # pool by default.
      name      = "builtin-mgr"
      namespace = one(kubernetes_namespace.rook.metadata).name
    }
    spec = {
      # The required pool name with underscores cannot be specified as a K8s resource name, thus we override
      # the pool name created in Ceph with this name property.
      # The ".mgr" pool is called "device_health_metrics" in Ceph versions v16.x.y and below.
      name          = ".mgr"
      failureDomain = "osd"
      replicated = {
        size                   = 2
        requireSafeReplicaSize = true
      }
    }
    parameters = {
      compression_mode = "none"
    }
    mirroring = {
      enabled = false
    }
  })
}
