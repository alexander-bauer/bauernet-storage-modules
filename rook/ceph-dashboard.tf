variable "ceph_dashboard_fqdns" {
  type = list(string)
}

resource "kubernetes_ingress_v1" "ceph_dashboard" {
  metadata {
    name      = "ceph-dashboard"
    namespace = one(kubernetes_namespace.rook.metadata).name
    annotations = {
      "cert-manager.io/cluster-issuer" = "letsencrypt"
    }
  }
  spec {
    dynamic "rule" {
      for_each = var.ceph_dashboard_fqdns
      content {
        host = rule.value
        http {
          path {
            path = "/"
            backend {
              service {
                name = "rook-ceph-mgr-dashboard"
                port {
                  number = 7000
                }
              }
            }
          }
        }
      }
    }
    tls {
      hosts       = var.ceph_dashboard_fqdns
      secret_name = "ceph-dashboard-tls"
    }
  }
}
