Storage
=======

This component configures storage services for consumption by other Kubernetes
services, and potentially for external consumption. This is quite a wide and
complex topic spanning a variety of layers and technologies.

The main storage technology is `Ceph <https://ceph.io/en/>`_, provided via
`Rook <https://rook.io/>`_. Ceph consumes raw disks or partitions, and makes
them available to the cluster as a whole via :abbr:`OSDs (object storage
daemons)`, with one such OSD per block device. Once OSDs are provisioned and
raw capacity is available, it can be made available by client use in a variety
of ways, of which three are in use here: block devices (:abbr:`RBD (RADOS block
device)`), networked filesystem (CephFS), and S3-compatible object storage
(:abbr:`RGW (RADOS Gateway)`).

Each of these technologies is made available to Kubernetes cluster using a
standard ``StorageClass`` (with corresponding :abbr:`CSI (Container Storage
Interface)` driver). Thus, normal Kubernetes pods (with no special configuration
for Ceph) are able to request Ceph-backed storage and have it dynamically
provisioned.

Backups are another crucial consideration for a highly available storage
fabric.  While it is possible for data backups to occur at the application
level, and may be appropriate in some cases, providing snapshot capabilities at
this layer allows us to build extremely capable low-or-no-downtime backup
systems on top, without the need for specific consideration for every
application on the cluster.

For each of RBD and CephFS, a ``VolumeSnapshotClass`` resource is configured,
so that a backup controller can orchestrate taking snapshots of either type of
pod storage.

.. mermaid::
  :caption: Resources at Play

  flowchart BT
    ceph_cluster[CephCluster]
    cephfs[CephFilesystem] -.-> ceph_cluster
    cephblockpool[CephBlockPool] -.-> ceph_cluster
    rgw[CephObjectStore] -.-> ceph_cluster
    cephfs_sc[StorageClass] -.-> cephfs
    cephfs_vsc[VolumeSnapshotClass] -.-> cephfs
    cephblockpool_sc[StorageClass] -.-> cephblockpool
    cephblockpool_vsc[VolumeSnapshotClass] -.-> cephblockpool

.. mermaid::
  :caption: Resource Consumption by other Systems

  flowchart LR
    subgraph Rook
      subgraph rook-ceph.rbd.csi.ceph.com
        rbdcsi["csi-rbdplugin"]
        rbdcsi_prov["csi-rbdplugin-provisioner"]
      end
      subgraph rook-ceph.cephfs.csi.ceph.com
        cephfscsi["csi-cephfsplugin"]
        cephfscsi_prov["csi-cephfsplugin-provisioner"]
      end
    end
    subgraph Ceph
      cephfs_underlying_volume["CephFS Directory + Quota"]
      rbd_underlying_volume["RBD Volume"]
    end
    subgraph Consumer
      cephfspod["Pod\nNeeding Shared Storage"]
      cephfspv["PV (CephFS)"]
      cephfspvc["PVC (CephFS)"]
      rbdpod["Pod\nNeeding Fast Storage"]
      rbdpv["PV (RBD)"]
      rbdpvc["PVC (RBD)"]
    end

    cephfspod --> cephfspvc
    cephfspvc -. Request .-> rook-ceph.cephfs.csi.ceph.com
    rook-ceph.cephfs.csi.ceph.com -. Provision .-> cephfspv & cephfs_underlying_volume
    cephfspvc -- Bind --> cephfspv
    cephfspv <-- Consume --> cephfs_underlying_volume

    rbdpod --> rbdpvc
    rbdpvc -. Request .-> rook-ceph.rbd.csi.ceph.com
    rook-ceph.rbd.csi.ceph.com -. Provision .-> rbdpv & rbd_underlying_volume
    rbdpvc -- Bind --> rbdpv
    rbdpv <-- Consume --> rbd_underlying_volume

Terragrunt Configuration
------------------------

.. literalinclude:: terragrunt.hcl
  :caption: ``terragrunt.hcl``

Terraform Configuration
-----------------------

.. todo:: Include Terraform config here.

How-To
------

Prepare Hosts and Disks for OSDs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In order for an entire disk to be consumed by Ceph, it must have its partition
table wiped. To do this for a target device ``/dev/sda``, use: ::

  dd if=/dev/zero of=/dev/sda bs=512 count=512

Inspect RGW Buckets with Minio Client
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Minio client (``mc``) is a provider-agnostic S3 CLI. This component creates
RGW administrator credentials and exposes them as a Terraform output. To ingest
this configuration into ``mc``, use the following command, where ``rgw`` is the
desired alias name.

.. code-block:: console

    $ terragrunt output -json | jq .object_admin_minio.value | mc alias import rgw
    Imported `rgw` successfully.
