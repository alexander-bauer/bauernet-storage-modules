resource "kubernetes_namespace" "rook" {
  metadata {
    name = "rook-ceph"
  }
}

resource "helm_release" "rook" {
  name       = "rook-ceph"
  repository = "https://charts.rook.io/release"
  chart      = "rook-ceph"
  namespace  = one(kubernetes_namespace.rook.metadata).name
  version    = "1.11.3"

  values = [
    jsonencode({
      monitoring = { enabled = true }
      crds       = { enabled = true }
      logLevel   = var.debug_operator == true ? "DEBUG" : "INFO"
    })
  ]
}
