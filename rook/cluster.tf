variable "ceph_nodes" {
  type = list(
    object({
      name = string,
      devices = list(
        object({
          name = string
        })
      )
    })
  )
}

resource "kubectl_manifest" "ceph_cluster" {
  depends_on = [helm_release.rook] # CRDs must be available first
  yaml_body = yamlencode({
    apiVersion = "ceph.rook.io/v1"
    kind       = "CephCluster"
    metadata = {
      name      = "ceph"
      namespace = one(kubernetes_namespace.rook.metadata).name
    }
    spec = {
      cephVersion = {
        image = "quay.io/ceph/ceph:v17"
      }
      dataDirHostPath = "/var/lib/rook"
      dashboard       = { enabled = true }
      mon = {
        count                = 1
        allowMultiplePerNode = false
      }
      storage = {
        useAllNodes           = false
        onlyApplyOSDPlacement = false
        nodes                 = var.ceph_nodes
      }
      monitoring = {
        enabled = false
      }
    }
  })

  wait = true

  lifecycle {
    prevent_destroy = true
  }
}
