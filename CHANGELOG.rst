#########
Changelog
#########

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[Unreleased]
============

[0.3.0] - 2023-08-13
====================

Added
-----

* Configure ``rook`` NFS Ganesha.

[0.2.0] - 2023-05-03
====================

Added
-----

* Migrated ``bucket`` module from ``bauernet-original``
* Migrated WIP ``backup`` module from ``bauernet-original``

[0.1.0] - 2023-05-02
====================

Added
-----

* Initial content migrated from ``bauernet-original``
