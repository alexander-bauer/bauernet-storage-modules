locals {
  bucket_name = coalesce(var.bucket, var.name)
}

resource "kubectl_manifest" "bucket" {
  yaml_body = yamlencode({
    apiVersion = "objectbucket.io/v1alpha1"
    kind       = "ObjectBucketClaim"
    metadata = {
      namespace = var.namespace
      name      = var.name
    }
    spec = {
      generateBucketName = local.bucket_name
      storageClassName   = var.storage_class
    }
  })
}

# Use kubectl wait directly to wait for the bucket to be "Bound" before moving
# on.
resource "null_resource" "wait_for_bucket_ready" {
  depends_on = [kubectl_manifest.bucket]
  triggers = {
    obc_uid = kubectl_manifest.bucket.uid
  }

  provisioner "local-exec" {
    command = join(" ", [
      "kubectl wait",
      "--for=jsonpath=.status.phase=Bound",
      "--namespace=${var.namespace}",
      "${kubectl_manifest.bucket.kind}/${kubectl_manifest.bucket.name}"
    ])
  }
}

data "kubernetes_config_map" "bucket" {
  depends_on = [null_resource.wait_for_bucket_ready]
  # When an OBC is answered, a corresponding config_map is created with the details
  # of the bucket.
  metadata {
    namespace = var.namespace
    name      = kubectl_manifest.bucket.name
  }
}

data "kubernetes_secret" "bucket" {
  depends_on = [null_resource.wait_for_bucket_ready]
  # When an OBC is answered, a corresponding secret is created with the details
  # of the bucket.
  metadata {
    namespace = var.namespace
    name      = kubectl_manifest.bucket.name
  }
}

locals {
  access = {
    name       = data.kubernetes_config_map.bucket.data.BUCKET_NAME
    host       = data.kubernetes_config_map.bucket.data.BUCKET_HOST
    port       = data.kubernetes_config_map.bucket.data.BUCKET_PORT
    access_key = data.kubernetes_secret.bucket.data.AWS_ACCESS_KEY_ID
    secret_key = data.kubernetes_secret.bucket.data.AWS_SECRET_ACCESS_KEY
    region     = coalesce(data.kubernetes_config_map.bucket.data.BUCKET_REGION, "default")
  }
}

output "access" {
  description = "Values for accessing the provisioned bucket"
  value       = local.access
}

output "resources" {
  description = "Kubernetes resources related to the bucket"
  value = {
    obc        = kubectl_manifest.bucket.name
    config_map = one(data.kubernetes_config_map.bucket.metadata).name
    secret     = one(data.kubernetes_secret.bucket.metadata).name
  }
}
