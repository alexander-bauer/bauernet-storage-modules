#############
Bucket Module
#############

This module is responsible for creating a local S3 bucket using RGW and emitting
its credentials.

How-To
======

Browse a Bucket Interactively
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Typically, persons do not have interactive credentials to RGW. Instead, service
credentials are created per-bucket, and passed only to applications which
require them. For interactive access, it is simplest to assume those same
credentials temporarily.

A popular toolset for interacting with S3-compatible interfaces in a
provider-agnostic way is the `Minio Client
<https://min.io/docs/minio/linux/reference/minio-mc.html>`_, typically called
(and invoked as) ``mc``. Credentials to specific providers in ``mc`` are called
"aliases," and behave similarly to AWS CLI "profiles." This is a oneliner for
loading an alias from an existing bucket, as created by this module.

.. code-block:: console

    $ export ALIAS=myalias
    $ export S3AP=https://<s3_access_point>
    $ export NAMESPACE=<bucket_namespace>
    $ export NAME=<bucket_symbolic_name>
    $ mc alias set "$ALIAS" "$S3AP" \
        "$(kubectl get -n $NAMESPACE secret $NAME -o json | jq -er '.data.AWS_ACCESS_KEY_ID | @base64d')" \
        "$(kubectl get -n $NAMESPACE secret $NAME -o json | jq -er '.data.AWS_SECRET_ACCESS_KEY | @base64d')"
