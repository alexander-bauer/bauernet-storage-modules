variable "namespace" {
  type        = string
  description = "Namespace in which to create bucket resources"
}

variable "name" {
  type        = string
  description = "Base name for resources"
}

variable "bucket" {
  type        = string
  description = "Base bucket name, if it should differ from ``name``"
  default     = null
}

variable "storage_class" {
  type        = string
  description = "Object storage class"
}
