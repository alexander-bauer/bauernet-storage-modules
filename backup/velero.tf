locals {
  backup_template = {
    # Defaults are largely fine. TODO: add fsfreeze hooks
  }
}

resource "helm_release" "velero" {
  name       = "velero"
  repository = "https://vmware-tanzu.github.io/helm-charts"
  chart      = "velero"
  namespace  = one(kubernetes_namespace.backup.metadata).name

  values = [
    yamlencode({
      initContainers = [
        {
          name            = "velero-plugin-for-csi"
          image           = "velero/velero-plugin-for-csi:v0.3.0"
          imagePullPolicy = "IfNotPresent"
          volumeMounts = [{
            mountPath = "/target"
            name      = "plugins"
          }]
        },
        {
          name            = "velero-plugin-for-aws"
          image           = "velero/velero-plugin-for-aws:v1.5.0"
          imagePullPolicy = "IfNotPresent"
          volumeMounts = [{
            mountPath = "/target"
            name      = "plugins"
          }]
        }
      ]
      configuration = {
        restoreOnlyMode        = var.restore_mode
        features               = "EnableCSI"
        provider               = "aws" # s3-compatible
        defaultVolumesToRestic = false
        backupStorageLocation = {
          # https://github.com/vmware-tanzu/velero-plugin-for-aws/blob/main/backupstoragelocation.md
          bucket = module.bucket.access.name
          prefix = "velero"
          config = {
            publicUrl        = var.local_bucket.external_url
            region           = module.bucket.access.region
            s3ForcePathStyle = true
            s3Url            = "http://${module.bucket.access.host}"
          }
        }
        volumeSnapshotLocation = {
          name     = "rbd"
          provider = var.rbd_snapshotclass
        }
      }
      deployNodeAgent = false
      schedules = {
        nightly = {
          disabled = false
          schedule = "0 0 * * 1-6" # every day except Sunday
          template = merge(local.backup_template, {
            ttl = "168h" # 7 days
          })
        }
        weekly = {
          disabled = false
          schedule = "0 0 * * 0" # only Sunday
          template = merge(local.backup_template, {
            ttl = "720h" # 30 days
          })
        }
      }
    }),
    yamlencode({
      credentials = {
        name      = "velero-local-bucket-credentials"
        key       = "credentials"
        useSecret = true
        secretContents = {
          cloud = <<-EOF
            [default]
            aws_access_key_id=${module.bucket.access.access_key}
            aws_secret_access_key=${module.bucket.access.secret_key}
          EOF
        }
      }
    })
  ]
}
