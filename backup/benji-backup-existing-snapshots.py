#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import sys

import kubernetes

import benji.k8s_tools.kubernetes
from benji.helpers.utils import setup_logging, logger

setup_logging()

VOLUMESNAPSHOT_TYPE = dict(
        group="snapshot.storage.k8s.io",
        version="v1",
        plural="VolumeSnapshots"
        )
VOLUMESNAPSHOTCONTENT_TYPE = dict(
        group="snapshot.storage.k8s.io",
        version="v1",
        plural="VolumeSnapshotContents"
        )

def main():
    # This arguments parser tries to mimic kubectl
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, allow_abbrev=False)

    parser.add_argument('-n',
                        '--namespace',
                        metavar='namespace',
                        dest='namespace',
                        default=None,
                        help='Filter on namespace')
    parser.add_argument('-l',
                        '--selector',
                        metavar='label-selector',
                        dest='labels',
                        action='append',
                        default=[],
                        help='Filter PVCs on label selector')
    parser.add_argument('--field-selector',
                        metavar='field-selector',
                        dest='fields',
                        action='append',
                        default=[],
                        help='Filter PVCs on field selector')
    parser.add_argument('--source-compare',
                        dest='source_compare',
                        action='store_true',
                        default=False,
                        help='Compare version to source after backup')

    args = parser.parse_args()

    benji.k8s_tools.kubernetes.load_config()
    core_v1_api = kubernetes.client.CoreV1Api()
    custom_objects_api = kubernetes.client.CustomObjectsApi()

    labels = ','.join(args.labels)
    fields = ','.join(args.fields)

    if args.namespace is not None:
        logger.info(f'Backing up all VolumeSnapshots in namespace {args.namespace}.')
    else:
        logger.info(f'Backing up all VolumeSnapshots in all namespaces.')
    if labels != '':
        logger.info(f'Matching label(s) {labels}.')
    if fields != '':
        logger.info(f'Matching field(s) {fields}.')

    if args.namespace is not None:
        volumesnapshots = custom_objects_api.list_namespaced_custom_object(
                namespace=args.namespace,
                field_selector=args.fields,
                label_selector=args.labels,
                watch=False,
                **VOLUMESNAPSHOT_TYPE
                )
    else:
        volumesnapshots = custom_objects_api.list_cluster_custom_object(
                field_selector=args.fields,
                label_selector=args.labels,
                watch=False,
                **VOLUMESNAPSHOT_TYPE
                )

    if len(volumesnapshots) == 0:
        logger.info("No matching VolumeSnapshots found")
        sys.exit(0)

    logger.debug("Found VolumeSnapshots", volumesnapshots=volumesnapshots)

if __name__ == "__main__":
    main()
