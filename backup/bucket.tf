module "bucket" {
  source = "../bucket"

  namespace     = one(kubernetes_namespace.backup.metadata).name
  name          = "backups-bucket"
  bucket        = "backups"
  storage_class = var.local_bucket.storage_class
}
