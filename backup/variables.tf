variable "restore_mode" {
  description = "Enable restore mode, preventing new backups"
  type        = bool
  default     = false
}

variable "local_bucket" {
  description = "Configuration for the local S3 bucket backup target"
  type = object({
    external_url  = string
    internal_url  = string
    storage_class = string
  })
}

variable "rbd_snapshotclass" {
  description = "VolumeSnapshotClass for rbd snapshots"
  type        = string
}
