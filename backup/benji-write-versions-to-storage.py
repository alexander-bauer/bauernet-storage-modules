#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import json
import sys
import time

import kubernetes

import benji.k8s_tools.kubernetes
from benji.helpers.utils import setup_logging, logger, subprocess_run
from benji.storage.factory import StorageFactory
from benji.config import Config

setup_logging()

def main():
    # Reusing the parser architecture from other scripts, for consistency.
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, allow_abbrev=False)

    parser.add_argument('-c', '--config-file', default=None, type=str, help='Specify a non-default configuration file')
    parser.add_argument('storage',
            metavar='target-storage',
            help='Storage to write versions list to')

    args = parser.parse_args()

    logger.debug('Loading config')
    if args.config_file is not None and args.config_file != '':
        try:
            cfg = open(args.config_file, 'r', encoding='utf-8').read()
        except FileNotFoundError:
            logger.error('File {} not found.'.format(args.config_file))
            sys.exit(os.EX_USAGE)
        config = Config(ad_hoc_config=cfg)
    else:
        config = Config()

    logger.debug(f'Looking up storage {args.storage}')
    StorageFactory.initialize(config)
    storage = StorageFactory.get_by_name(args.storage)
    if not hasattr(storage, '_write_object') or not callable(storage._write_object):
        logger.critical(f'Storage {args.storage} does not support _write_object()')
        sys.exit(1)
    logger.debug(f'Got storage: {storage}')

    response = json.loads(subprocess_run([
        'benji', '--machine-output', 'ls'
        ]))
    versions = response['versions']

    logger.info(f'Received report of {len(versions)} versions')
    logger.debug(f'Response from Benji CLI', versions=versions)

    t1 = time.time()
    report_name = f'reports/report-{t1}'
    logger.debug(f'Using report name: {report_name}')

    # Try/except/raise pattern from src/benji/storage/base.py
    try:
        logger.debug(f'Attempting to write object {report_name}')
        byte_data = json.dumps(response).encode('utf-8')
        storage._write_object(key=report_name, data=byte_data)
    except:
        try:
            logger.debug(f'Failed to write object {report_name}, deleting')
            self._rm_object(key=report_name)
        except FileNotFoundError:
            pass
        raise

    logger.info(f'Wrote version report {report_name}')

if __name__ == '__main__':
    main()
