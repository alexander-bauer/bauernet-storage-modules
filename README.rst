########################
BauerNet Storage Modules
########################

This repository contains Terraform modules responsible for the configuration of the
persistent storage layer on Kubernetes for the BauerNet infrastructure.

.. toctree::
   :maxdepth: 1

   */README
   CHANGELOG
